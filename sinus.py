import random
from ai import NetworkConfiguration, init, Matrix
from ai.preparation import shuffle
import math

random.seed(1)

training_loss_list = []
test_loss_list = []

# training loss -> Fehler während des Trainings
# test loss -> Fehler nach dem Training
def save_loss_callback(epoch, training_loss, test_loss):
  training_loss_list.append(training_loss)
  test_loss_list.append(test_loss)
  
def show_training_progress(epoch, training_loss, test_loss):
  print("Training Loss: {}, Test Loss: {}\n".format(training_loss, test_loss))

# generate training data
training_inputs = []
training_outputs = []

samples = 500
for i in range(samples):
  training_inputs.append([7*i/samples])
  training_outputs.append([math.sin(training_inputs[-1][0])])

training_inputs, training_outputs = shuffle(training_inputs, training_outputs)

for i in range(len(training_inputs)):
  training_inputs[i] = Matrix.fromList(training_inputs[i])
  training_outputs[i] = Matrix.fromList(training_outputs[i])

config = NetworkConfiguration()
config.configureInputLayer(1)
config.addHiddenLayer(7, "sigmoid")
config.addHiddenLayer(7, "sigmoid")
config.configureOutputLayer(1, "identity")

network = config.compile(init.gauss)

network.train(training_inputs, training_outputs, 200, 0.2, save_loss_callback)
network.save("sinus.ai")

# show plot
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 7, 50)

inputs = np.array([[x[i]] for i in range(len(x))])
outputs = [network.predict(Matrix.fromList(input)) for input in inputs]
sin = [math.sin(x[i]) for i in range(len(x))]

fig = plt.figure()
ax = plt.axes()
ax.scatter([x[0] for x in inputs], [output[0] for output in outputs])
ax.scatter([x[0] for x in inputs], sin)
ax.legend(["Network Output", "Sinus"])

# show loss
fig = plt.figure()
ax = plt.axes()
ax.plot(test_loss_list)
ax.plot(training_loss_list)
ax.legend(["Test Loss", "Training Loss"])

plt.show()
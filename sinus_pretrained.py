from ai import Network, Matrix

network = Network.load("sinus.ai")

# show plot
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 7, 50)

inputs = np.array([[x[i]] for i in range(len(x))])
outputs = [network.predict(Matrix.fromList(input)) for input in inputs]

fig = plt.figure()
ax = plt.axes()
ax.scatter([x[0] for x in inputs], [output[0] for output in outputs])
plt.show()
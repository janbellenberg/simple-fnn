import random
from ai import NetworkConfiguration, init, Matrix

random.seed(1)

training_loss_list = []
test_loss_list = []

# training loss -> Fehler während des Trainings
# test loss -> Fehler nach dem Training
def save_loss_callback(epoch, training_loss, test_loss):
  training_loss_list.append(training_loss)
  test_loss_list.append(test_loss)
  
def show_training_progress(epoch, training_loss, test_loss):
  print("Training Loss: {}, Test Loss: {}\n".format(training_loss, test_loss))

training_inputs = [Matrix.fromList([0, 0]), Matrix.fromList([0, 1]), Matrix.fromList([1, 0]), Matrix.fromList([1, 1])]
training_outputs = [Matrix.fromList([0]), Matrix.fromList([1]), Matrix.fromList([1]), Matrix.fromList([0])]

config = NetworkConfiguration()
config.configureInputLayer(2)
config.addHiddenLayer(5, "sigmoid")
config.configureOutputLayer(1, "identity")

network = config.compile(init.gauss)

network.train(training_inputs, training_outputs, 400, 0.2, save_loss_callback, training_inputs, training_outputs)
network.save("xor.ai")

print("0 0 -> ", network.predict(training_inputs[0]).content)
print("0 1 -> ", network.predict(training_inputs[1]).content)
print("1 0 -> ", network.predict(training_inputs[2]).content)
print("1 1 -> ", network.predict(training_inputs[3]).content)

# show 3D plot
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 1, 50)
y = np.linspace(0, 1, 50)

inputs = np.array([[x[i], y[j]] for i in range(len(x)) for j in range(len(y))])
outputs = [network.predict(Matrix.fromList(input)) for input in inputs]

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.scatter3D([x[0] for x in inputs], [x[1] for x in inputs], [output[0] for output in outputs])

# show loss
fig = plt.figure()
ax = plt.axes()
ax.plot(test_loss_list)
ax.plot(training_loss_list)
ax.legend(["Test Loss", "Training Loss"])

plt.show()
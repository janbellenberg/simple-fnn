from ai import Network, Matrix, NetworkConfiguration, init
from ai.preparation import shuffle
import math

network1 = Network.load("sinus.ai")

training_inputs = []
training_outputs = []
samples = 20
for i in range(samples):
  training_inputs.append([7*i/samples])
  training_outputs.append([math.cos(training_inputs[-1][0])])

training_inputs, training_outputs = shuffle(training_inputs, training_outputs)

for i in range(len(training_inputs)):
  training_inputs[i] = Matrix.fromList(training_inputs[i])
  training_outputs[i] = Matrix.fromList(training_outputs[i])

network1.train(training_inputs, training_outputs, 5, 0.2, None)


config = NetworkConfiguration()
config.configureInputLayer(1)
config.addHiddenLayer(7, "sigmoid")
config.addHiddenLayer(7, "sigmoid")
config.configureOutputLayer(1, "identity")

network2 = config.compile(init.gauss)

network2.train(training_inputs, training_outputs, 5, 0.2)


# show plot
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 7, 50)

inputs = np.array([[x[i]] for i in range(len(x))])
outputs1 = [network1.predict(Matrix.fromList(input)) for input in inputs]
outputs2 = [network2.predict(Matrix.fromList(input)) for input in inputs]

fig = plt.figure()
ax = plt.axes()
ax.scatter([x[0] for x in inputs], [output[0] for output in outputs1])
ax.scatter([x[0] for x in inputs], [output[0] for output in outputs2])
ax.legend(["Pre-Trained", "Untrained"])
plt.show()
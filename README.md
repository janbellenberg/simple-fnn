# Feedforward neural network

Implementation of a simple feedforward neural network and the backpropagation algorithm for training. 

> Copyright © Jan Bellenberg 2023
import random

def gauss(mu=0, sigma=2) -> int:
  return random.gauss(mu, sigma)

def standard(middle=0, range=1) -> int:
  return middle + random.random()*2*range - range
import math

def sigmoid(x):
  return 1 / (1 + math.exp(-x))

def sigmoid_derivative(x):
  return sigmoid(x) * (1 - sigmoid(x))

def identity(x):
  return x

def identity_derivative(x):
  return 1


activations = {
  "sigmoid": (sigmoid, sigmoid_derivative),
  "identity": (identity, identity_derivative)
}
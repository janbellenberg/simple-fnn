from ai.matrix import Matrix
from ai.activation import activations

class Network:
  layers = None
  training_complete = False

  def __init__(self, layers) -> None:
    self.layers = layers

  def __str__(self) -> str:
    if self.layers == None:
      return "Network not initialized"
    
    network_overview = "\n".join(map(lambda layer: str(layer), self.layers))
    return "Network with {} layers [summary]:\n{}".format(len(self.layers), network_overview)
  
  def __repr__(self) -> str:
    return str(self)
  
  def predict(self, inputs, layer_callback = None, override_training_warn = False):
    if not self.training_complete and not override_training_warn:
      print("WARNING: Network not completely trained yet")

    # pass through input layer
    layer_output = inputs
    if layer_callback != None:
      layer_callback((layer_output, layer_output))
      
    # calculate hidden layers & output layer
    for layer in self.layers:
      layer_inputs = layer_output << Matrix.ones(1, 1)  # add bias
      layer_sums = layer_inputs @ ~layer.weights       # multiply weights
      layer_output = layer_sums.foreach(lambda x: activations[layer.activation][0](x))            # apply activation function

      if layer_callback != None:
        layer_callback((layer_sums, layer_output))

    return layer_output

  def train(self, inputs, outputs, epochs, learning_rate, epoch_callback = None, test_inputs = None, test_outputs = None):
    
    for epoch in range(epochs):
      print("Epoch: {}".format(epoch+1))

      training_errors = []
      for (input, output) in zip(inputs, outputs):
        # predict & calc error
        outputs_per_layer = []
        network_output = self.predict(input, lambda result: outputs_per_layer.append(result), True)
        error = output - network_output
        training_errors.append(self._mse(output, network_output))
        
        # calculate gradients
        gradients = []
        for i in reversed(range(len(self.layers))):
          (layer_sums, _) = outputs_per_layer[i+1]   # output of hidden layer (layer0) is at index 1
          layer_activation_deriv = activations[self.layers[i].activation][1]    # derivative of activation function

          gradient = error * layer_sums.foreach(lambda x: layer_activation_deriv(x))
          gradients.insert(0, gradient) # insert at beginning, because we are going backwards

          if i > 0:   # dont calculate error, if there is no next layer
            weights_without_bias = self.layers[i].weights.truncate(self.layers[i].weights.rows(), self.layers[i].weights.columns() - 1)
            error = gradients[0] @ weights_without_bias

        # update weights
        for i in range(len(self.layers)):
          (_, layer_inputs) = outputs_per_layer[i]   # i=0 => outputs of input layer -> input of hidden layer
          layer_inputs = layer_inputs << Matrix.ones(1, 1)  # add bias to layer inputs

          # create matrix for each weight by multiplying each gradient (one per neuron) with each input
          delta_weights = (~gradients[i] @ layer_inputs) * learning_rate      # calculate change for each weight based on input and learning rate
          self.layers[i].weights = self.layers[i].weights + delta_weights   # update weights
      
      # calculate training loss
      training_loss = sum(training_errors) / len(training_errors)

      test_loss = None
      if test_inputs is not None and test_outputs is not None:
        test_errors = []
        for (input, output) in zip(test_inputs, test_outputs):
          network_output = self.predict(input, None, True)
          test_errors.append(self._mse(output, network_output))
        test_loss = sum(test_errors) / len(test_errors)
        
      if epoch_callback != None:
        epoch_callback(epoch, training_loss, test_loss)

    self.training_complete = True

  def save(self, path):
    with open(path, "w") as file:
      file.write(
        "\n".join(map(lambda layer: str(layer.weights) + "@" + layer.activation, self.layers))
      )

  @staticmethod
  def load(path):
    from .layer import Layer
    from .matrix import Matrix
    with open(path, "r") as file:
      lines = file.readlines()
      layers = []
      for line in lines:
        line = line.replace("\n", "").replace("\r", "")

        # decode line
        (weights, activation) = line.split("@")
        weights = list(map(lambda row: list(map(float, row.split(","))), weights.split(";")))

        # convert weights to matrix
        weightMatrix = Matrix(len(weights), len(weights[0]))
        for i in range(len(weights)):
          for j in range(len(weights[0])):
            weightMatrix[i][j] = weights[i][j]

        # create layer
        layers.append(Layer(weightMatrix, activation))

      network = Network(layers) 
      network.training_complete = True
      return network
    
  def _mse(self, target, output):
    tmp = (target - output) ** 2
    mse = 0

    for i in range(tmp.rows()):
      for j in range(tmp.columns()):
        mse += tmp[i][j]
    
    return mse / (tmp.rows() * tmp.columns())

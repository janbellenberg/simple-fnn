import random

def shuffle(inputs, outputs):
  # Shuffle the inputs and outputs in the same order
  shuffled_inputs = []
  shuffled_outputs = []
  examples = len(inputs)
  for _ in range(examples):
    index = random.randrange(len(inputs))
    shuffled_inputs.append(inputs[index])
    shuffled_outputs.append(outputs[index])
    inputs.pop(index)
    outputs.pop(index)

  return shuffled_inputs, shuffled_outputs

def split(data, ratio):
  data = shuffle(data)
  split_index = int(len(data) * ratio)
  return data[:split_index], data[split_index:]

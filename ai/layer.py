class Layer:
  weights = None  
  activation = None

  def __init__(self, weights, activation) -> None:
    self.weights = weights
    self.activation = activation

  @staticmethod
  def configure(inputs, neurons, init_strategy, activation):
    from .matrix import Matrix
    weights = Matrix(neurons, inputs)
    
    for i in range(neurons):
      for j in range(inputs):
        weights[i][j] = init_strategy()

    return Layer(weights, activation)

  def __str__(self) -> str:
    return "Layer with {} neurons and {} inputs".format(self.weights.rows(), self.weights.columns())
  
  def __repr__(self) -> str:
    return str(self)
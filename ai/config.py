class NetworkConfiguration:
  inputs = None
  outputs = None
  hidden_layers = []

  def configureInputLayer(self, inputs):
    self.inputs = inputs

  def configureOutputLayer(self, outputs, activation):
    self.outputs = (outputs, activation)

  def addHiddenLayer(self, neurons, activation):
    self.hidden_layers.append((neurons, activation))

  def compile(self, init_strategy):
    from .layer import Layer
    from .network import Network

    layers = []
    
    # +1 for biases
    layer_inputs = self.inputs
    for layer in self.hidden_layers:
      (neurons, activation) = layer
      layers.append(Layer.configure(layer_inputs+1, neurons, init_strategy, activation))
      layer_inputs = neurons

    (output_neurons, output_activation) = self.outputs
    layers.append(Layer.configure(layer_inputs+1, output_neurons, init_strategy, output_activation))

    return Network(layers)
  
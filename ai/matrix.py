class Matrix:
  content = []

  @staticmethod
  def ones(rows, columns):
    result = Matrix(rows, columns)
    for i in range(rows):
      for j in range(columns):
        result[i][j] = 1
    return result
  
  @staticmethod
  def zeros(rows, columns):
    result = Matrix(rows, columns)
    for i in range(rows):
      for j in range(columns):
        result[i][j] = 0
    return result
  
  @staticmethod
  def fromList(list):
    # build row vector
    result = Matrix(1, len(list))
    for i in range(len(list)):
      result[0][i] = list[i]
    return result
  
  @staticmethod
  def fromArray2D(array):
    result = Matrix(len(array), len(array[0]))
    for i in range(len(array)):
      for j in range(len(array[0])):
        result[i][j] = array[i][j]
    return result

  def __init__(self, rows, columns) -> None:
    self.content = [[0 for _ in range(columns)] for _ in range(rows)]

  def rows(self) -> int:
    return len(self.content)
  
  def columns(self) -> int:
    return len(self.content[0])

  def __str__(self) -> str:
    return ";".join(map(lambda row: ",".join(map(str, row)), self.content))
  
  def __repr__(self) -> str:
    return str(self)
  
  def __getitem__(self, key):
    return self.content[key]
  
  def __setitem__(self, key, value):
    self.content[key] = value

  def foreach(self, func):
    result = Matrix(self.rows(), self.columns())
    for i in range(self.rows()):
      for j in range(self.columns()):
        result[i][j] = func(self.content[i][j])

    return result

  def truncate(self, rows, columns):
    if rows > self.rows() or columns > self.columns():
      raise Exception("Cannot truncate matrix to {} rows and {} columns".format(rows, columns))

    result = Matrix(rows, columns)
    for i in range(rows):
      for j in range(columns):
        result[i][j] = self.content[i][j]
    return result

  def __add__(self, other):
    if isinstance(other, Matrix):
      if self.rows() == other.rows() and self.columns() == other.columns():
        result = Matrix(self.rows(), self.columns())
        for i in range(self.rows()):
          for j in range(self.columns()):
            result[i][j] = self.content[i][j] + other.content[i][j]
        return result
      else:
        raise Exception("Matrixes have different dimensions")
    else:
      raise TypeError("unsupported operand type(s) for +: Matrix and '{}'".format(type(other)))
      
  def __sub__(self, other):
    if isinstance(other, Matrix):
      if self.rows() == other.rows() and self.columns() == other.columns():
        result = Matrix(self.rows(), self.columns())
        for i in range(self.rows()):
          for j in range(self.columns()):
            result[i][j] = self.content[i][j] - other.content[i][j]
        return result
      else:
        raise Exception("Matrixes have different dimensions")
    else:
      raise TypeError("unsupported operand type(s) for -: Matrix and '{}'".format(type(other)))

  def __pow__(self, exponent):
    if isinstance(exponent, int):
      result = Matrix(self.rows(), self.columns())
      for i in range(self.rows()):
        for j in range(self.columns()):
          result[i][j] = self.content[i][j] ** exponent
      return result
    else:
      raise TypeError("unsupported operand type(s) for ** or pow(): Matrix and '{}'".format(type(exponent)))
  
  def __lshift__(self, other):
    if isinstance(other, Matrix):
      if self.rows() == other.rows():
        result = Matrix(self.rows(), self.columns() + other.columns())
        for i in range(self.rows()):
          # copy current matrix
          for j in range(self.columns()):
            result[i][j] = self.content[i][j]
          # append other matrix
          for j in range(other.columns()):
            result[i][j + self.columns()] = other.content[i][j]

        return result
      else:
        raise Exception("Matrixes have different row count")
    else:
      raise TypeError("unsupported operand type(s) for <<: Matrix and '{}'".format(type(other)))
    
  
  def __mul__(self, other):
    if isinstance(other, int) or isinstance(other, float):
      # SCALAR
      result = Matrix(len(self.content), len(self.content[0]))
      for i in range(len(self.content)):
        for j in range(len(self.content[0])):
          result[i][j] = self.content[i][j] * other
      return result
    elif isinstance(other, Matrix):
      if self.columns() == other.columns() and self.rows() == other.rows():
        # ELEMENTWISE
        result = Matrix(self.rows(), self.columns())
        for i in range(self.rows()):
          for j in range(self.columns()):
            result[i][j] = self.content[i][j] * other.content[i][j]
        return result
      else:
        raise Exception("Matrixes have different dimensions")
    else:
      raise TypeError("unsupported operand type(s) for *: Matrix and '{}'".format(type(other)))
    
  def __matmul__(self, other):
    if isinstance(other, Matrix):
      if self.columns() == other.rows():
        result = Matrix(self.rows(), other.columns())
        for i in range(self.rows()):
          for j in range(other.columns()):
            for k in range(other.rows()):
              result[i][j] += self.content[i][k] * other.content[k][j]
        return result
      else:
        raise Exception("Matrix multiplication is not possible")
    else:
      raise TypeError("unsupported operand type(s) for @: Matrix and '{}'".format(type(other)))

  # transpose matrix
  def __invert__(self):
    result = Matrix(self.columns(), self.rows())
    for i in range(self.rows()):
      for j in range(self.columns()):
        result[j][i] = self.content[i][j]
    
    return result
  
  def rows(self):
    return len(self.content)

  def columns(self):
    return len(self.content[0])
import numpy
from matplotlib import pyplot
from ai import Matrix
import random

random.seed(1)

data_file = open("mnist_train_100.csv", 'r')
data_list = data_file.readlines()
data_file.close()

training_inputs = []
training_outputs = []

for example in data_list:
    all_values = example.split(',')
    inputs = (numpy.asfarray(all_values[1:])/255.0*0.99)+0.01
    outputs = numpy.zeros(10)+0.01
    outputs[int(all_values[0])] = 0.99
    training_inputs.append(Matrix.fromList(inputs))
    training_outputs.append(Matrix.fromList(outputs))

data_file = open("mnist_test_10.csv", 'r')
data_list = data_file.readlines()
data_file.close()

test_inputs = []
test_outputs = []

for example in data_list:
    all_values = example.split(',')
    inputs = (numpy.asfarray(all_values[1:])/255.0*0.99)+0.01
    outputs = numpy.zeros(10)+0.01
    outputs[int(all_values[0])] = 0.99
    test_inputs.append(Matrix.fromList(inputs))
    test_outputs.append(Matrix.fromList(outputs))


from ai import NetworkConfiguration, init

training_loss_list = []
test_loss_list = []

config = NetworkConfiguration()
config.configureInputLayer(784)
config.addHiddenLayer(100, "sigmoid")
config.configureOutputLayer(10, "sigmoid")

network = config.compile(lambda: init.gauss(0, 0.5))
#print(network.layers[1].weights)

network.train(training_inputs, training_outputs, 50, 0.3)
#print(training_inputs[0])
#print(network.predict(training_inputs[0]))
network.save("handwrite.ai")

def argmax(list):
    max = 0
    max_index = 0
    for i in range(len(list)):
        if list[i] > max:
            max = list[i]
            max_index = i
    return max_index

correct = 0
for i in range(len(training_inputs)):
    output = network.predict(training_inputs[i])
    
    if argmax(output.content[0]) == argmax(training_outputs[i].content[0]):
        correct += 1

print(correct)

correct = 0
for i in range(len(test_inputs)):
    output = network.predict(test_inputs[i])
    
    if argmax(output.content[0]) == argmax(test_outputs[i].content[0]):
        correct += 1

print(correct)